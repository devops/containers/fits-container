# FITS Container

FITS project: https://projects.iq.harvard.edu/fits/home

## Build

    $ docker build -t fits .

## Run

Print usage:

    $ docker run --rm fits

Run FITS on file at `path/to/my/data/file.txt`:

    $ docker run --rm -v "/path/to/my/data:/data" fits -i /data/file.txt

Pass custom Java options:

	$ docker run --rm -e FITS_JAVA_OPTS="-Xmx4g" -v "/path/to/my/data:/data" fits -i /data/file.txt

## Copy into another build

Example for Debian:

	# Set Java options
	ENV FITS_JAVA_OPTS="-Djava.io.tmpdir=/var/tmp/fits -Xmx4g"

	# Install dependencies (Java 8 minimum)
    RUN apt-get -y install file mediainfo openjdk-11-jre-headless

	# Copy FS layer
	COPY --from=gitlab-registry.oit.duke.edu/devops/containers/fits-container:main /usr/src/fits /usr/src/fits

	# Optional - symlink to PATH
	RUN ln -s /usr/src/fits/fits.sh /usr/local/bin/fits.sh
