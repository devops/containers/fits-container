FROM debian:bullseye

ARG fits_version=1.5.5
ENV fits_path=/usr/src/fits

SHELL ["/bin/bash", "-c"]

RUN set -eux; \
	apt-get -y update; \
	apt-get -y upgrade; \
	apt-get -y install \
	# Download dependencies
	curl unzip \
	# FITS dependencies
	file mediainfo openjdk-11-jre-headless \
	; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

WORKDIR /tmp
RUN set -eux; \
	curl -s -O https://automation.lib.duke.edu/fits/fits-${fits_version}.zip; \
	curl -s https://automation.lib.duke.edu/fits/fits-${fits_version}.zip.sha512 \
	    | sha512sum -c - ; \
	unzip -q fits-${fits_version}.zip -d ${fits_path}

WORKDIR $fits_path
RUN set -eux; \
	cp /usr/lib/x86_64-linux-gnu/libmediainfo.so.0 \
	    /usr/lib/x86_64-linux-gnu/libzen.so.0 \
	    ./tools/mediainfo/linux/; \
	# Disable logging to file
	sed -E -i 's/^(log4j\.appender\.FILE\.Threshold=).*$/\1OFF/' ./log4j.properties; \
	sed -E -i 's/^(JAVA_OPTS=).*$/JAVA_OPTS="\${FITS_JAVA_OPTS}"/' ./fits-env.sh; \
	# smoke test
	./fits.sh -v

VOLUME /data

ENTRYPOINT [ "./fits.sh" ]

CMD [ "-h" ]
