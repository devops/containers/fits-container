SHELL = /bin/bash

build_tag ?= fits

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) - < ./Dockerfile

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	docker run --rm -v "$(shell pwd)/test:/data" $(build_tag) -i /data/test.jpg
